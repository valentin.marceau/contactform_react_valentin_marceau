import logo from './logo.svg';
import './App.css';
import React, { createContext, useState, ReactNode, useContext, useEffect } from 'react';
import $ from 'jquery';

function App() {

  return (
    <div className="App">
      <Contact />
    </div>
  );
}

const LoadContext = createContext();

function LoadContextProvider({ children }: { children: ReactNode }) {
  const [action, setAction] = useState(0);
  const [fields, setFields] = useState(0);
  const [search, setSearch] = useState("");

  return (
    <LoadContext.Provider
      value={{
        action,
        fields,
        search,
        newAction: (content) => setAction(action => content),
        editFields: (content) => setFields(fields => content),
        setSearch: (content) => setSearch(search => content)
      }}
    >
    {children}
    </LoadContext.Provider>
  )
}

function Contact() {
  return (
    <LoadContextProvider>
    <ContactFields />
    <ContactList />
    <br />
    <ContactForm />
    </LoadContextProvider>
  )
}

function ContactFields() {

  const searchValue = useContext(LoadContext);

  const search = (event) => {
    searchValue.setSearch(event.target.value);
    searchValue.newAction(searchValue.action + 1);
  }

  const newContact = () => {
    searchValue.editFields(0);
    $(".contact_form").animate({
      width: '40%',
      opacity: '1'
    });
  }

  return (
    <header className="App-header">
    <div>
      <input placeholder="Recherche..." className="search_input borderless_input" type='search' onChange={search} aria-label='Annuaire de contact :' />
    </div>

    <div onClick={newContact} className="new_contact">
      <p>Nouveau contact</p>
    </div>
    </header>
  )
}

function ContactList() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const update = useContext(LoadContext);

  const editContact = (id, name, sirname, email, birthdate, e) => {
    update.editFields([id, name, sirname, email, birthdate]);
    if(!$(".contact_action_delete").has(e.target).length && !$(".contact_action_delete").is(e.target) && !$(".modal_confirm").has(e.target).length && !$(".modal_confirm").is(e.target)) {
      $(".contact_form").animate({
        width: '40%',
        opacity: '1'
      });
    }
  }
  const displayModal = (id) => {
    $('#modal_confirm_'+id).show();
  }
  const deleteContact = (id) => {
    var data = "";

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        update.newAction(update.action + 1);
      }
    });

    xhr.open("DELETE", "http://localhost:3004/contact/"+id);
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("postman-token", "88c7c35a-c3c7-6622-f1e5-6b9ca4f3ea28");

    xhr.send(data);
  }
  const hideModal = (id) => {
    $('#modal_confirm_'+id).hide();
  }

  useEffect(() => {
    if(!update.search) {
      fetch("http://localhost:3004/contact")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          if(items != result) {
            setItems(result);
          }
        },
        (error) => {
          console.log(error);
          setIsLoaded(true);
          setError(error);
        }
      )
    }
    else {
      fetch("http://localhost:3004/contact?q="+update.search)
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          if(items != result) {
            setItems(result);
          }
        },
        (error) => {
          console.log(error);
          setIsLoaded(true);
          setError(error);
        }
      )
    }
  }, [update.action])

  if(update.search) {
    var text = "Aucun contact ne correspond à la recherche";
  }
  else {
    var text = "Aucun contact pour l'instant";
  }

  if(error) {
    return (
      <div className="Contact container">
      <p>erreur {error}</p>
      </div>
    );
  }
  else if (!isLoaded) {
    return <p> pas chargé </p>;
  }
  else {
    if(items && items.length > 0) {
      return (
        <div className="contact_list">
        {items.map(item => (
          <div key={item.id} onClick={(e) => editContact(item.id, item.name, item.sirname, item.email, item.birthdate, e)} className="contact">
            <div className="contact_infos" >
              <div className="contact_header">
                <p>{item.name}</p>
                <p>{item.sirname}</p>
              </div>
              <div className="contact_email">
                <p>{item.email}</p>
              </div>
              <div className="contact_birthdate">
                <p>{item.birthdate}</p>
              </div>
            </div>
            <div className="contact_action">
              <div className="contact_action_delete">
                <svg onClick={() => displayModal(item.id)} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
                  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                  <path fillRule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
              </div>
              <div id={"modal_confirm_"+ item.id} style={{display: "none"}} className="modal_confirm">
                <p>Supprimer ce contact ?</p>
                <div className='modal_confirm_buttons'>
                  <button value="yes" onClick={() => deleteContact(item.id)}>Supprimer</button>
                  <button value="no" onClick={() => hideModal(item.id)}>Annuler</button>
                </div>
              </div>
            </div>
          </div>
        ))}
        </div>
      );
    }
    else {
      return (
        <div className='no_results'>
          {text}
        </div>
      );
    }
  }
}

function ContactForm() {
  const [action, setAction] = useState("add");

  const [name, setName] = useState('');
  const [sirname, setSirname] = useState('');
  const [email, setEmail] = useState('');
  const [birthdate, setBirthdate] = useState('');

  const [errorName, setErrorName] = useState('');
  const [errorSirname, setErrorSirname] = useState('');
  const [errorEmail, setErrorEmail] = useState('');
  const [errorBirthdate, setErrorBirthdate] = useState('');
  const [disabled, setDisabled] = useState('');

  const regexName = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~\d]/;
  const regexEmail = /[ `!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/;
  const regexEmailAfterSend = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const regexBirthdate = /[ `!#$%^&*()_+\-=\[\]{};':"\\|,.<>?~a-zA-Z]/;
  const regexBirthdateAfterSend = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

  const update = useContext(LoadContext);

  useEffect(() => {
    if(update.fields != 0) {
      setName(update.fields[1]);
      setSirname(update.fields[2]);
      setEmail(update.fields[3]);
      setBirthdate(update.fields[4]);
      setErrorName('');
      setErrorSirname('');
      setErrorEmail('');
      setErrorBirthdate('');
      setDisabled('');
      setAction('edit');
    }
    else {
      setName("");
      setSirname("");
      setEmail("");
      setBirthdate("");
      setErrorName('');
      setErrorSirname('');
      setErrorEmail('');
      setErrorBirthdate('');
      setDisabled('');
      setAction('add');
    }
  }, [update.fields])

  const change = (event) => {
    switch (event.target.name) {
      case 'name':
        if(regexName.test(event.target.value)) {
          setName(event.target.value);
          setErrorName('Seules les lettres et les espaces sont autorisé');
          setDisabled('true');
          break;
        }
        else {
          setName(event.target.value);
          setErrorName('');
          setDisabled('');
          break;
        }
      case 'sirname':
        if(regexName.test(event.target.value)) {
          setSirname(event.target.value);
          setErrorSirname('Seules les lettres et les espaces sont autorisé');
          setDisabled('true');
          break;
        }
        else {
          setSirname(event.target.value);
          setErrorSirname('');
          setDisabled('');
          break;
        }
      case 'email':
        if(regexEmail.test(event.target.value)) {
          setEmail(event.target.value);
          setErrorEmail('Adresse email non valide');
          setDisabled('true');
          break;
        }
        else {
          setEmail(event.target.value);
          setErrorEmail('');
          setDisabled('');
          break;
        }
      case 'birthdate':
        if(regexBirthdate.test(event.target.value)) {
          setBirthdate(event.target.value);
          setErrorBirthdate('Date non valide');
          setDisabled('true');
          break;
        }
        else {
          setBirthdate(event.target.value);
          setErrorBirthdate('');
          setDisabled('');
          break;
        }
    }
  };

  const submitForm = (event) => {

    var data = "name="+name+"&sirname="+sirname+"&email="+email+"&birthdate="+birthdate+"";

    if (name == "" || sirname == "" || email == "" || birthdate == "" || !regexEmailAfterSend.test(email) || !regexBirthdateAfterSend.test(birthdate)) {
      if(name == "") {
        setErrorName("Ce champ ne peut pas être vide");
      }
      if(sirname == "") {
        setErrorSirname("Ce champ ne peut pas être vide");
      }
      if(email == "") {
        setErrorEmail("Ce champ ne peut pas être vide");
      }
      else if (!regexEmailAfterSend.test(email)) {
        setErrorEmail("Le mail doit être de la forme exemple@mail.fr");
      }
      if(birthdate == "") {
        setErrorBirthdate("Ce champ ne peut pas être vide");
      }
      else if (!regexBirthdateAfterSend.test(birthdate)) {
        setErrorBirthdate("La date doit être au format jj/mm/aaaa");
      }
    }
    else {
      var xhr = new XMLHttpRequest();
      xhr.withCredentials = true;

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          update.newAction(update.action + 1);
        }
      });

      if(action == "add") {
        xhr.open("POST", "http://localhost:3004/contact");
        $(".contact_form").animate({
          width: '25%',
          opacity: '0'
        });
        setName('');
        setSirname('');
        setEmail('');
        setBirthdate('');
      }
      else {
        xhr.open("PUT", "http://localhost:3004/contact/"+update.fields[0]);
      }

      xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
      xhr.setRequestHeader("cache-control", "no-cache");
      xhr.setRequestHeader("postman-token", "aecbb92d-dbb9-65f8-5eca-9e5300e73214");

      xhr.send(data);
    }
    event.preventDefault();

  };

  if(action == "add") {
    var header = <h2 className='contact_form_header'>Ajout d'un contact</h2>;
    var button_text = "Ajouter";
  }
  else {
    var header = <h2 className='contact_form_header'>Modification d'un contact</h2>;
    var button_text = "Modifier";
  }
    return (
      <div className="contact_form container">
        <form>
        {header}
        <div className="cross">
        &#10005;
        </div>
          <div className="contact_form_name">
            <label>
              <input placeholder="Prénom" className="borderless_input" value={name} onChange={change} type='text' name='name' />
              <p className="contact_form_error">{errorName}</p>
            </label>
            <label>
              <input placeholder="Nom" className="borderless_input" value={sirname} type='text' onChange={change} name='sirname' />
              <p className="contact_form_error">{errorSirname}</p>
            </label>
          </div>
          <div className="contact_form_email">
            <label>
              <input placeholder="Email" className="borderless_input" value={email} onChange={change} type='text' name='email' />
              <p className="contact_form_error">{errorEmail}</p>
            </label>
          </div>
          <div className="contact_form_birthdate">
            <label>
              <input placeholder='Date de naissance' className="borderless_input" type='text' value={birthdate}  onChange={change} name='birthdate' />
              <p className="contact_form_error">{errorBirthdate}</p>
            </label>
          </div>
          <input className="submitForm" disabled={disabled} type='submit' value={button_text} name='submit' onClick={submitForm} />
        </form>
      </div>
    );
}

$('body').on("click", function(event) {
  if(!$(".modal_confirm").is(event.target) && !$(".modal_confirm").has(event.target).length && !$(".contact_action_delete").is(event.target) && !$(".contact_action_delete").has(event.target).length) {
    $(".modal_confirm").each(function() {
      if($(this).is(':visible')) {
        $(this).hide();
      }
    });
  }
  if(!$(".contact_form").is(event.target) && !$(".contact_form").has(event.target).length && !$(".contact").is(event.target) && !$(".contact").has(event.target).length && !$(".new_contact").is(event.target) && !$(".new_contact").has(event.target).length) {
    $(".contact_form").animate({
      width: '25%',
      opacity: '0'
    });
  }
  else if($('.cross').is(event.target) || $(".cross").has(event.target).length) {
    $(".contact_form").animate({
      width: '25%',
      opacity: '0'
    });
  }
});

export default App;
